﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebServer.Services;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("")]
    public class HomeController : ControllerBase
    {
        private readonly IWebHostEnvironment _environment;
        private readonly ICustomFileReader _fileReader;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IWebHostEnvironment environment, ICustomFileReader fileReader,
            ILogger<HomeController> logger)
        {
            _environment = environment;
            _fileReader = fileReader;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] string filename)
        {
            if (string.IsNullOrEmpty(filename) || filename.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                return BadRequest();
            }

            var path = Path.Combine(_environment.ContentRootPath, "Files", filename);

            if (!System.IO.File.Exists(path))
            {
                _logger.LogWarning($"File {path} not found");
                return NotFound();
            }

            try
            {
                var bytes = await _fileReader.ReadAsync(path);
                return File(bytes, "text/plain", filename);
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception, "Critical error", new { Path = path });
                throw;
            }
        }
    }
}
