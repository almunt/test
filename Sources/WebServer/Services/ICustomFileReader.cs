﻿using System.Threading.Tasks;

namespace WebServer.Services
{
    public interface ICustomFileReader
    {
        Task<byte[]> ReadAsync(string filePath);
    }
}