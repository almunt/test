﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;

namespace WebServer.Services
{
    public class CustomFileReader : ICustomFileReader
    {
        private readonly ConcurrentDictionary<string, Task<byte[]>> _cache =
            new ConcurrentDictionary<string, Task<byte[]>>();

        public async Task<byte[]> ReadAsync(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(filePath));

            return await _cache.GetOrAdd(filePath, ReadFileAsync);
        }

        private async Task<byte[]> ReadFileAsync(string path)
        {
            var bytes = await File.ReadAllBytesAsync(path);
            await Task.Delay(TimeSpan.FromSeconds(2)); //emulate "long" file reading
            _cache.TryRemove(path, out _);
            return bytes;
        }
    }
}